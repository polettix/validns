# DNS validation

Do some validations for DNS resolutions.

```
validns foo.example.com bar.example.org > result.json
```

The following checks are performed:

- retrieval of basic information from the relevant Registry, possibly via RDAP,
  using a WHOIS API provider as a fallback (API key mostly needed here)
- retrieval of delegation in DNS
- comparison of nameserver lists from Registry and delegation in DNS
- comparison of SOA records across all authoritative nameservers
- comparison of nameserver lists across all authoritative nameservers
- availability of one IPv4 address for each authoritative nameserver or any
  other namerserver that is depended upon.

The result is printed as a JSON array of objects in standard output, each
object carrying the results for one input domain.

The object contains:

- a `status` key that can be either `ok` or `error`
- if there are errors, an `errors` key associated to an array of errors
- a `checks` key, associated to an array of checks (the list above), each
  represented by an object.

The object for each sub-check always contains a `name`, the `status` and, in
case of errors, a list in an array pointed by key `errors`. There might be
other data specific to the test and collected data too.

Example run on `example.com`:

```json
[
   {
      "checks" : [
         {
            "auth_registry" : {
               "changed" : "2023-05-12T15:13:35Z",
               "domain" : "example.com",
               "expires" : "2023-08-13T04:00:00Z",
               "nameservers" : [
                  "a.iana-servers.net",
                  "b.iana-servers.net"
               ],
               "rdap" : "https://rdap.verisign.com/com/v1/",
               "source" : "rdap",
               "status" : [
                  "client delete prohibited",
                  "client transfer prohibited",
                  "client update prohibited"
               ]
            },
            "name" : "authority data in registry",
            "status" : "ok"
         },
         {
            "auth_dns" : {
               "branch" : "example.com.",
               "domain" : "example.com",
               "nameserver" : [
                  "a.iana-servers.net",
                  "b.iana-servers.net"
               ]
            },
            "name" : "authority data in DNS",
            "status" : "ok"
         },
         {
            "name" : "Registry/DNS correpondence",
            "status" : "ok"
         },
         {
            "name" : "SOA alignment",
            "soa" : {
               "a.iana-servers.net" : [
                  "example.com.   3600   IN   SOA   ( ns.icann.org. noc.dns.icann.org.",
                  "            2022091302   ;serial",
                  "            7200      ;refresh",
                  "            3600      ;retry",
                  "            1209600      ;expire",
                  "            3600      ;minimum",
                  "   )"
               ],
               "b.iana-servers.net" : [
                  "example.com.   3600   IN   SOA   ( ns.icann.org. noc.dns.icann.org.",
                  "            2022091302   ;serial",
                  "            7200      ;refresh",
                  "            3600      ;retry",
                  "            1209600      ;expire",
                  "            3600      ;minimum",
                  "   )"
               ]
            },
            "status" : "ok",
            "warnings" : [
               {
                  "a.iana-servers.net (a.iana-servers.net)" : "SOA primary NS not in NS list"
               },
               {
                  "b.iana-servers.net (b.iana-servers.net)" : "SOA primary NS not in NS list"
               }
            ]
         },
         {
            "name" : "NS correspondence",
            "nss" : {
               "a.iana-servers.net" : [
                  "a.iana-servers.net",
                  "b.iana-servers.net"
               ],
               "b.iana-servers.net" : [
                  "a.iana-servers.net",
                  "b.iana-servers.net"
               ]
            },
            "status" : "ok"
         },
         {
            "addresses" : {
               "a.iana-servers.net" : "199.43.135.53",
               "a.icann-servers.net" : "199.43.135.53",
               "b.iana-servers.net" : "199.43.133.53",
               "b.icann-servers.net" : "199.43.133.53",
               "c.iana-servers.net" : "199.43.134.53",
               "c.icann-servers.net" : "199.43.134.53",
               "ns.icann.org" : "199.4.138.53"
            },
            "name" : "NS resolution to IPv4",
            "status" : "ok"
         }
      ],
      "domain" : "example.com",
      "name" : "Checks for example.com",
      "status" : "ok"
   }
]
```

## Using WHOIS API providers

As it seems, [RDAP is the new WHOIS][post], but many TLD registries are
lagging a bit and so consuming a WHOIS API might be the only chance left.
`validns` supports three providers, whose configuration is provided with
command-line option `-t`:

- *you yourself*, placing JSON files named like `example.com.json` all in a
  directory, and passing a command-line option like `-t
  localcache:/path/to/that/directory`

- [whoisjson.com][], with a free tier (as of 2023-06-23) of 500 requests per
  month. Pass command-line parameter `-t whoisjson:$WHOISJSON_API_KEY`.

- [ip2location.io][], with a free tier (as of 2023-06-23) of 500 requests
  per month. Pass command-line parameter `-t ip2location:$IP2LOCATION_API_KEY`

Other API providers can be implemented in `ValiDNS::RegistrationInfo.pm`, if
you're *that kind* of person.

It's possible to pass option `-t` multiple times; these providers will be
tried in sequence as they appear in the command line.

## Using a configuration file

This is mostly useful to place API keys for WHOIS. Create a JSON file in a
directory:

```json
{
    "tokens": [
        [ "whoisjson", "7ADDA7ADDA7ADDA", "force-update"],
        "ip2location:F00B44E",
        [ "localcache", "/path/to/some/directory" ]
    ]
}
```

The token can be passed as a string, in the same format as the command-line
option, or as an array of values, the first being the specific source for
WHOIS data, and other as parameters for that specific source.


# COPYRIGHT & LICENSE

The contents of this repository are licensed according to the Apache
License 2.0 (see file `LICENSE` in the project's root directory):

>  Copyright 2023 by Flavio Poletti (flavio@polettix.it).
>
>  Licensed under the Apache License, Version 2.0 (the "License");
>  you may not use this file except in compliance with the License.
>  You may obtain a copy of the License at
>
>      http://www.apache.org/licenses/LICENSE-2.0
>
>  Unless required by applicable law or agreed to in writing, software
>  distributed under the License is distributed on an "AS IS" BASIS,
>  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  See the License for the specific language governing permissions and
>  limitations under the License.


[post]: https://etoobusy.polettix.it/2023/06/20/rdap-is-the-new-whois/
[whoisjson.com]: https://whoisjson.com/
[ip2location.io]: https://www.ip2location.io/
