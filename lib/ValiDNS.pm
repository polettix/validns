#!/usr/bin/env perl
package ValiDNS;

use v5.24;
use warnings;

use Scalar::Util 'blessed';
use Net::DNS;
use Ouch ':trytiny_var';
use Log::Any ();
use Data::Dumper;
use Try::Catch;
use Path::Tiny;
use Digest;
use JSON::PP 'decode_json';

use Moo;
use experimental 'signatures';
use ValiDNS::RegistrationInfo;
use ValiDNS::Outcome;

sub __ensure_ending_dot ($domain) { $domain =~ s{\.*\z}{.}rmxs }
sub __rooted   ($domain) { lc($domain =~ s{\.*\z}{.}rmxs) }
sub __unrooted ($domain) { lc($domain =~ s{\.+\z}{}rmxs) }

sub __subsumes ($reference, $target) {
   $_ = __ensure_ending_dot($_) for ($reference, $target);
   return !!($target =~ m{(?: \A | \.) \Q$reference\E \z}mxs);
}

sub __gist ($data) {
   state $encoder = JSON::PP->new->ascii->canonical->shrink;
   state $digester = Digest->new('SHA-1');
   my $json = $encoder->encode($data);
   my $digest  = $digester->reset->add($json)->hexdigest;
   return join '-', $digest, $json; # "fail fast"
}

sub __soa_gist ($soa) {
   state $encoder = JSON::PP->new->ascii->canonical;
   return fc(
      __gist(
         {
            map { $_ => $soa->can($_)->($soa) }
               qw < owner mname rname serial refresh retry expire minimum >
         }
      )
   );
}

sub __deq ($d1, $d2) {
   fc(__ensure_ending_dot($d1)) eq fc(__ensure_ending_dot($d2));
}

sub __filter_reply ($reply, @types) {
   my %is_requested = map { $_ => 1 } @types;
   my %records;
   for my $method (qw< answer authority additional >) {
      for my $rr ($reply->$method) {
         my $type = uc($rr->type);
         next unless $is_requested{$type};
         push(($records{$type}{$method} //= [])->@*, $rr);
         push(($records{$type}{all} //= [])->@*, [$method => $rr]);
      }
   }
   return \%records;
}

sub __split_reply_string ($reply) {
   $reply = $reply->string if ref($reply);
   return [ split m{\n}mxs, $reply =~ s{\t}{   }rgmxs ];
}

use namespace::clean;

with 'ValiDNS::Role::Logger';

has _registration_info => (is => 'lazy', init_arg => 'registration_info',
   coerce => \&__ri_coerce);
has _nscache => (is => 'lazy', init_arg => 'nscache', clearer => 1);
has _cache => (is => 'lazy', init_arg => 'cache', clearer => 1);

has registry_auth_ns_path => (is => 'ro', default => undef);

sub _build__registration_info ($self) { return {} }
sub __ri_coerce ($ri) {
   return $ri if blessed($ri); # already an object, assume it's fine
   return ValiDNS::RegistrationInfo->new($ri);
}

sub _build__nscache ($self) {
   my $basic = {
      '.' => [
         'f.root-servers.net.',
         'i.root-servers.net.',
         'b.root-servers.net.',
         'k.root-servers.net.',
         'l.root-servers.net.',
         'm.root-servers.net.',
         'j.root-servers.net.',
         'c.root-servers.net.',
         'd.root-servers.net.',
         'a.root-servers.net.',
         'e.root-servers.net.',
         'g.root-servers.net.',
         'h.root-servers.net.',
      ],
      'net.' => [
         'l.gtld-servers.net.',
         'b.gtld-servers.net.',
         'm.gtld-servers.net.',
         'f.gtld-servers.net.',
         'a.gtld-servers.net.',
         'g.gtld-servers.net.',
         'i.gtld-servers.net.',
         'd.gtld-servers.net.',
         'c.gtld-servers.net.',
         'e.gtld-servers.net.',
         'h.gtld-servers.net.',
         'k.gtld-servers.net.',
         'j.gtld-servers.net.',
      ],
      'org.' => [
         'b0.org.afilias-nst.org.',
         'b2.org.afilias-nst.org.',
         'a0.org.afilias-nst.info.',
         'd0.org.afilias-nst.org.',
         'c0.org.afilias-nst.info.',
         'a2.org.afilias-nst.info.',
      ],
   };
   $basic->{'com.'} = $basic->{'net.'};
   return $basic;
}

sub _nscache_closer_to ($self, $domain) {
   my $cache = $self->_nscache;
   my $level = $domain = __ensure_ending_dot($domain);
   while (length($level)) {
      last if exists $cache->{$level};
      $level =~ s{\A [^.]* \. }{}mxs; # chop, rinse & repeat
   }
   $level = '.' unless length($level);
   return($level, $cache->{$level});
}

sub _nscache_add ($self, $domain, $value) {
   $self->_nscache->{__ensure_ending_dot($domain)} = $value;
}

sub _build__cache ($self) { return {} }

sub _query ($self, %args) {
   $self->DEBUG(Dumper({_query => \%args}));

   my ($from_cache, $to_cache) =
      map { exists($args{$_}) ? delete($args{$_}) : 1 }
      qw< from_cache to_cache >;
   
   my ($key, $cache);
   if ($from_cache || $to_cache) {
      $cache = $self->_cache;
      $key = __gist(
         {
            query => {
               resolver => $args{resolver},
               args     => $args{args},
            }
         }
      );
   }

   my $reply;
   if ($from_cache && exists($cache->{$key})) {
      $reply = $cache->{$key};
   }
   else {
      my $resolver = Net::DNS::Resolver->new(
         debug   => 0,
         recurse => 0,
         retrans => 3,
         retry   => 1,
         $args{resolver}->%*,
      );
      $resolver->udp_timeout(3);
      $reply = $resolver->send($args{args}->@*);
      ouch 500, 'no reply', { resolver => $resolver, args => \%args }
        unless defined $reply;
      $cache->{$key} = $reply if $to_cache;
   }

   return $reply unless wantarray;

   my $rrs;
   $rrs = __filter_reply($reply, $args{filter}->@*) if $args{filter};
   return ($reply, $rrs);
}

sub __render_ns ($ns) {
   return $ns unless ref($ns);
   my ($name, @ips) = $ns->@*;
   my $ips = @ips > 1 ? ('[' . join(', ', @ips) . ']') : $ips[0];
   return "$name/$ips";
}

sub dns_auth_nameservers_for ($self, $domain) {
   $self->DEBUG("dns_auth_nameservers_for($domain)");

   my $edomain = __ensure_ending_dot($domain);

   my ($level, $nss) = $self->_nscache_closer_to($edomain);
   $self->DEBUG("  starting at <$level>");

   if (! __deq($level, $edomain)) {
      my @nss = $nss->@*;
      while ('necessary') {
         $self->DEBUG("  searching at <$level>");

         my $nslist;
         for my $ns (@nss) {
            local $" = ', ';
            my $item = __render_ns($ns);
            $nslist = defined($nslist) ? "$nslist, $item" : $item;
            $self->DEBUG('    NS: ' . $item);
         }

         my ($reply, $rrs) = $self->_query(
            resolver => {
               nameserver => [ map { ref($_) ? $_->[1] : $_ } @nss ],
            },
            args => [$domain, 'SOA'],
            filter => [ qw< SOA NS A > ],
         );
         $self->TRACE($reply->string);

         last if exists($rrs->{SOA});

         my $rcode = $reply->header->rcode;
         ouch 500, "error: $rcode ($level from ($nslist))", $reply
            if $rcode ne 'NOERROR';

         @nss = $rrs->{NS}{authority}->@*
            or ouch 500, "no authoritative NSs provided", $reply;
         $level = __ensure_ending_dot($nss[0]->owner);

         my %address_for = map { $_->[1]->owner => $_->[1]->address }
            ($rrs->{A}{all} // [])->@*;
         @nss = map {
            my $name = $_->nsdname;
            $address_for{$name} ? [ $name, $address_for{$name} ] : $name;
         } @nss;

         $self->_nscache_add($level, [@nss]);
      }
      $nss = \@nss;
   }

   return {
      domain => $domain,
      branch => $level,
      nameserver => [ $nss->@* ],
   };
}

sub check_nameservers_for ($self, $domain) {
   $self->INFO("checking nameservers for $domain");

   my $outcome = $self->_outcome("Checks for $domain",
      'Suite of checks for a domain, see details for each individual one',
      domain => $domain,
      checks => \my @checks);

   # get basic data from registry and DNS
   push @checks, (my $auth_reg = $self->_check_registry_auth($domain));
   push @checks, (my $auth_dns = $self->_check_dns_auth($domain));

   if (defined(my $adns = $auth_dns->{auth_dns})) {
      if (defined(my $areg = $auth_reg->{auth_registry})) {
         push @checks, $self->_check_registry($areg, $adns);
      }
      push @checks,
         $self->_check_glue_records($adns),
         $self->_check_soa_mismatches($adns),
         $self->_check_ns_mismatches($adns),
         $self->_check_ns_ipv4_resolvable($adns);
   }

   for my $check (@checks) {
      $outcome->record_error({ $check->{name} => $check->{description} })
         if $check->{status} ne 'ok';
   }

   $self->INFO("  $domain summary: " . ($outcome->status eq 'ok' ? 'OK' : 'ERROR'));

   return $outcome->as_hash;
}

sub _check_ns_mismatches ($self, $auth) {
   $self->INFO('  NS correspondence');
   my $description = <<'END';
Check each nameserver in the list of authoritative nameservers (as per
upper domain in DNS) to see if it carries the exact same list of
authoritative nameservers. This misalignment might happen if nameservers
are changed only locally in a domain's zone file, but this change is not
communicated up to the upper domain that sets the actual delegation.
END
   my $outcome = $self->_outcome('NS correspondence', $description,
      nss => \my %nss_for);

   my $nss = $auth->{nameserver};
   my $expected = [ map { ref($_) ? $_->[0] : $_ } $nss->@* ];

   for my $ns ($nss->@*) {
      my ($name, $address) = ref($ns) ? $ns->@* : ($ns, $ns);
      $self->DEBUG("    NS: $name ($address)");

      my $reply;
      try {
         ($reply, my $rrs) = $self->_query(
            resolver => { nameserver => [ $address ] },
            args => [ $auth->{branch}, 'NS' ],
            filter => [ qw< NS A > ],
         );
         $self->TRACE($reply->string);

         ouch 400, 'SERVFAIL'
            if uc($reply->header->rcode) eq 'SERVFAIL';

         # check nameservers correspondence
         $nss_for{$name} = my $got =
            [ map { $_->nsdname } ($rrs->{NS}{answer} // [])->@* ];
         my ($unexpected, $missing) = __sets_compare($got, $expected);

         $outcome->record_error(
            {
               ns => $ns,
               description => 'unexpected authoritative nameservers',
               list => $unexpected,
               reply => __split_reply_string($reply),
            }
         ) if $unexpected->@*;

         $outcome->record_error(
            {
               ns => $ns,
               description => 'missing authoritative nameservers',
               list => $missing,
               reply => __split_reply_string($reply),
            }
         ) if $missing->@*;
      }
      catch {
         my $message = bleep();
         my $query = "$name ($address)";
         my $data = $query;
         if ($reply) {
            $data = { query => $query };
            $data->{reply} = __split_reply_string($reply);
         }
         $self->ERROR("      $message $query");
         $outcome->record_error({$message => $data});
         $nss_for{$name} = undef;
      };
   }

   return $outcome->as_hash;
}

sub _check_soa_mismatches ($self, $auth) {
   $self->INFO('  SOA alignment');
   my $description = <<'END';
Ensure that all authoritative nameservers (as per upper delegating
nameserver in DNS) have the same SOA record. This might show misalignments
for secondary servers not updating after a change in the primary, e.g.
because the sequence number was not incremented.
END
   my $outcome = $self->_outcome('SOA alignment', $description,
      soa => \my %soa_for);
   my %flag_for;
   my $nss = $auth->{nameserver};
   my %is_nss = map { __rooted(ref($_) ? $_->[0] : $_) => 1 } $nss->@*;
   for my $ns ($nss->@*) {
      my ($name, $address) = ref($ns) ? $ns->@* : ($ns, $ns);
      $self->DEBUG("    NS: $name ($address)");

      my $reply;
      try {
         ($reply, my $rrs) = $self->_query(
            resolver => { nameserver => [ $address ] },
            args => [ $auth->{branch}, 'SOA' ],
            filter => [ 'SOA' ],
         );
         $self->TRACE($reply->string);

         ouch 400, 'SERVFAIL'
            if uc($reply->header->rcode) eq 'SERVFAIL';

         my $soa = $rrs->{SOA}{all}[0][1];
         $flag_for{__soa_gist($soa)} = 1;
         $soa_for{$name} = __split_reply_string($soa);

         my $mname = $soa->mname;
         if (! $is_nss{__rooted($mname)}) {
            my $message = 'SOA primary NS not in NS list';
            my $query = "$name ($address)";
            $self->WARN("      $message for $query");
            $outcome->record_warning( { $query => $message });
         }
      }
      catch {
         my $message = bleep();
         my $query = "$name ($address)";
         my $data = $query;
         if ($reply) {
            $data = { query => $query };
            $data->{reply} = __split_reply_string($reply);
         }
         $self->ERROR("      $message $query");
         $outcome->record_error({$message => $data});
         $soa_for{$name} = undef;
      };

   }

   $outcome->record_error('different SOA values')
      if scalar(keys(%flag_for)) > 1;
   return $outcome->as_hash;
}

sub _check_ns_ipv4_resolvable ($self, $auth) {
   $self->INFO('  NS resolution to IPv4');
   my $description = <<'END';
Ensure that all nameservers (as per upper delegating nameserver in DNS)
can be resolved to IPv4 addresses. This includes glue records, that are
taken as being more authoritative than what might be written "inside".
END
   my $outcome = $self->_outcome('NS resolution to IPv4', $description,
      addresses => \my %address_for);
   my %auth_for = ($auth->{domain} => $auth, $auth->{branch} => $auth);

   sub ($auth, $level = 0) {
      my ($domain, $branch) = $auth->@{qw< domain branch >};
      $self->DEBUG("    (level $level) looking into $branch for $domain");

      # collect addresses for nameservers
      my @delayed_nss;
      for my $ns ($auth->{nameserver}->@*) {
         if (ref($ns)) {
            my ($name, $address) = $ns->@*;
            $self->DEBUG("    NS: $name ($address), has glue record, OK");
            $address_for{$name} = $address;
         }
         elsif (defined($address_for{$ns})) {
            $self->DEBUG("    IPv4 for $ns already learned");
         }
         elsif (__subsumes($branch, $ns)) {
            $self->WARN("    missing glue record/resolution for $ns");
            $outcome->record_error({ $ns => 'no glue record/resolution' });
         }
         else {
            $self->DEBUG("    NS: $ns, needs further investigation");
            push @delayed_nss, $ns;
         }
      }

      for my $ns (@delayed_nss) {
         my $sub_auth = $self->dns_auth_nameservers_for($ns);
         if ($level < 4) {
            __SUB__->($sub_auth, $level + 1);
         }
         else {
            $self->ERROR("$ns: deep recursion?");
            $outcome->record_error({ $ns => 'deep recursion?!?' });
         }
      }

      # nothing more to do at the top level
      return unless $level;

      # otherwise, we have to resolve the $domain, which is related to a
      # nameserver that is needed by the caller
      for my $ns ($auth->{nameserver}->@*) {
         my $ns_address = ref($ns) ? $ns->[1]
            : exists($address_for{$ns}) ? $address_for{$ns}
            : next;
         my ($reply, $rrs) = $self->_query(
            resolver => { nameserver => [ $ns_address ] },
            args => [$domain, 'A'],
            filter => [ qw< A > ],
         );
         if (my ($item) = ($rrs->{A}{all} // [])->@*) {
            $address_for{$domain} //= $item->[1]->address;
         }
         else {
            $self->ERROR("    $ns cannot resolve $domain");
            $outcome->record_error({ $ns => "cannot resolve $domain" });
         }
      }
   }->($auth);

   return $outcome->as_hash;
}

sub __sets_compare ($s1, $s2) {
   my %h1 = ref($s1) eq 'HASH' ? $s1->%* : map { $_ => 1 } $s1->@*;
   my %h2 = ref($s1) eq 'HASH' ? $s2->%* : map { $_ => 1 } $s2->@*;
   my @h2_only;
   for my $k2 (keys(%h2)) {
      if (exists($h1{$k2})) { delete $h1{$k2}    }
      else                  { push @h2_only, $k2 }
   }
   return([sort keys(%h1)], [sort @h2_only]);
}

sub _check_registry_auth ($self, $domain) {
   $self->INFO('  retrieving authority in registry');
   my $outcome = $self->_outcome('authority data in registry', <<'END');
Retrieve authority data in Registry, by any available mean (RDAP, WHOIS).
This might fail if e.g. the remote server is throttling us, is down, or
we don't have permission (e.g. for hitting an upper limit of allowed
queries).
END
   try {
      my $ar = $self->registry_auth_nameservers_for($domain);
      $outcome->record_meta(auth_registry => $ar);
   }
   catch {
      $self->ERROR(bleep);
      $outcome->record_error(bleep());
   };
   return $outcome->as_hash;
}

sub _check_dns_auth ($self, $domain) {
   $self->INFO('  retrieving authority in DNS');
   my $outcome = $self->_outcome('authority data in DNS', <<'END');
Retrieve authority data from the DNS infrastructure, moving from the root
down to the upper delegating nameserver. This also retrieves any glue
record that might have been set in the upper delegating nameserver.
END
   try {
      my $ad = $self->dns_auth_nameservers_for($domain);
      $outcome->record_meta(auth_dns => $ad);
   }
   catch {
      $self->ERROR(bleep());
      $outcome->record_error(bleep());
   };
   return $outcome->as_hash;
}

sub ___items ($input) {
   return unless defined $input;
   return ref($input) ? $input->@* : $input;
}

sub ___first ($input) { ref($input) ? $input->[0] : $input }

sub ___filter_to_aref ($in, $cb) { [ map { $cb->($_) } ___items($in) ] }

sub ___filter_hosts ($in) { ___filter_to_aref($in, \&___first) }

sub _check_registry ($self, $auth_registry, $auth_dns) {
   $self->INFO('  Registry/DNS correspondence');
   my $outcome = $self->_outcome('Registry/DNS correspondence', <<'END');
Compare the authority sections from the Registry (via RDAP/WHOIS) and from
the DNS. Misalignments here might either be transient, or indicate that
something wrong is happening, e.g. a domain has been obscured (so it's not
in the DNS but is present in the Registry).
END
   $outcome->record_meta(auth_dns => $auth_dns);
   $outcome->record_meta(auth_registry => $auth_registry);

   my ($dns_only, $reg_only) = __sets_compare(
      ___filter_hosts($auth_dns->{nameserver}),
      ___filter_hosts($auth_registry->{nameservers}),
   );
   $outcome->record_error({ 'in DNS but not in Registry' => $dns_only })
      if $dns_only->@*;
   $outcome->record_error({ 'in Registry but not in DNS' => $reg_only })
      if $reg_only->@*;

   return $outcome->as_hash;
}

sub registry_auth_nameservers_for ($self, $domain) {
   if (-e (my $path = $self->registry_auth_ns_path // '')) {
      return decode_json(path($path)->slurp_raw);
   }
   return $self->_registration_info->fetch($domain);
}

sub _check_glue_records ($self, $auth_dns) {
   my $outcome = $self->_outcome('Glue records in upper domain', <<'END');
Check that the IP address of any glue record is actually a nameserver and
that it is authoritative for the given domain too (this is checked by
querying for the SOA record). An error here mostly indicates that the glue
record is wrong, e.g. because provider changes made it obsolete; most of
the time the Registrar should be asked to update this value in the Registry
according to the correct IP address of the name needing the glue record.
END
   $self->INFO('  ' . $outcome->name);
   $outcome->record_meta(auth_dns => $auth_dns);
   my $domain = $auth_dns->{domain};
   for my $ns ($auth_dns->{nameserver}->@*) {
      next unless ref($ns);
      my ($nsname, $ip) = $ns->@*;
      my $title = "glue record $nsname -> $ip (for $domain)";
      $self->INFO('    ' . $title);
      my $reply;
      try {
         ($reply, my $rrs) = $self->_query(
            resolver => { nameserver => [ $ip ] },
            args => [$domain, 'SOA'],
            filter => [ qw< SOA NS A > ],
         );
         $self->TRACE($reply->string);

         ouch 400, 'SERVFAIL'
            if uc($reply->header->rcode) eq 'SERVFAIL';

         ouch 400, "No SOA for $domain"
            unless defined($rrs->{SOA}{all}[0][1])
      }
      catch {
         my $message = bleep();
         $self->ERROR("      $message");
         $outcome->record_error(
            {
               'failed glue record' => "$nsname -> $ip",
               error => $message,
               ($reply ? (reply => __split_reply_string($reply)) : ()),
            }
         );
      };
   }
   return $outcome->as_hash;
}

sub _outcome ($self, $name, $description = undef, %data) {
   return ValiDNS::Outcome->new(
      name => $name,
      description => $description // 'No description',
      meta => \%data,
   );
}

1;
