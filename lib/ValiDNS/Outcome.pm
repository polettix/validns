#!/usr/bin/env perl
package ValiDNS::Outcome;

use v5.24;
use warnings;
use Storable 'dclone';

use Moo;
use experimental 'signatures';

use namespace::clean;

has meta    => (is => 'ro', default  => sub { return {} });
has name    => (is => 'ro', required => 1);
has description => (is => 'ro', default => 'No description available');
has _errors => (is => 'ro', init_arg => undef, default => sub { [] });
has _warns  => (is => 'ro', init_arg => undef, default => sub { [] });

sub as_hash ($self, %stuff) {
   return {
      $self->meta->%*, %stuff,
      name   => $self->name,
      description => $self->description,
      status => $self->status,
      ($self->has_errors   ? (errors => dclone($self->_errors))   : ()),
      ($self->has_warnings ? (warnings => dclone($self->_warns)) : ()),
   };
} ## end sub as_hash

sub errors       ($self)         { return dclone($self->_errors)->@* }
sub has_errors   ($self)         { return scalar($self->_errors->@*) > 0 }
sub has_warnings ($self)         { return scalar($self->_warns->@*) > 0 }
sub record_error ($self, $item)  { push $self->_errors->@*, $item; $self }
sub record_meta  ($self, $k, $v) { $self->meta->{$k} = $v }
sub record_warning ($self, $it)  { push $self->_warns->@*, $it; $self }
sub status         ($self)       { $self->has_errors ? 'error' : 'ok' }

1;
