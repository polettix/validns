#!/usr/bin/env perl
package ValiDNS::RegistrationInfo::WhoisIt;

use v5.24;
use warnings;
use Moo;
use experimental qw< signatures >;
use namespace::clean;

extends 'ValiDNS::RegistrationInfo::WhoisBase';
has '+host'     => (default => 'whois.nic.it');
has '+_accepts' => (default => '(?imxs: \.it \z)');

sub _parse_data ($self, $data) {
   my @lines = split m{\x{0d}?\x{0a}}mxs, $data;
   my $top = undef;
   my $sub = undef;
   my @nss;
   my %value_for;
   for my $line (@lines) {
      next if $line =~ m{\A \s* (?: \* | \z)}mxs;

      # at the top level we always look for a key and infer a value
      if ($line =~ m{\A \S}mxs) {
         $top = $sub = undef;
         my ($key, $value) = $self->_kvcheck($line);
         if (! defined($value)) {
            $top = $key;
         }
         else {
            $value_for{$key} = $value;
         }
      }
      else { # indented level
         next unless defined($top);
         $line = $self->_trim($line);

         my $reftype = ref($value_for{$top});
         if (! $reftype) { # infer!
            $sub = undef;
            my ($key, $value) = $self->_kvcheck($line);
            if (defined($value)) {
               $value_for{$top} = {$key => $value};
               $sub = $key;
            }
            else {
               $value_for{$top} = [ $line ];
            }
         }
         elsif ($reftype eq 'ARRAY') {
            push $value_for{$top}->@*, $self->_trim($line);
         }
         elsif ($reftype eq 'HASH') {
            my ($key, $value) = $self->_kvcheck($line);
            if (defined($value)) {
               $value_for{$top}{$key} = $value;
               $sub = $key;
            }
            else {
               $value_for{$top}{$sub} .= "\n" . $line;
            }
         }
         else { ... } # should never happen!
      }
   }

   $value_for{nameservers} = $value_for{Nameservers};
   return \%value_for;
}

1;
