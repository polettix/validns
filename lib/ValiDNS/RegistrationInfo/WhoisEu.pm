#!/usr/bin/env perl
package ValiDNS::RegistrationInfo::WhoisEu;

use v5.24;
use warnings;
use Moo;
use experimental qw< signatures >;
use namespace::clean;

extends 'ValiDNS::RegistrationInfo::WhoisBase';
has '+host' =>     (default => 'whois.eu');
has '+_accepts' => (default => '(?imxs: \.eu \z)');

sub _parse_data ($self, $data) {
   my @lines = split m{\x{0d}?\x{0a}}mxs, $data;
   my $top = undef;
   my @nss;
   my %value_for;
   for my $line (@lines) {
      next if $line =~ m{\A \s* (?: % | \z)}mxs;
      
      # at the top level we always look for a key and infer a value
      if ($line =~ m{\A \S}mxs) {
         my ($key, $value) = $self->_kvcheck($line);
         if (! defined($value)) {
            # skip this... no ":" found!
         }
         elsif (length($value)) {
            $value_for{$key} = $value;
            $top = undef; # reset
         }
         else {
            $top = $key;
         }
      }
      else { # indented level
         next unless defined($top);
         $line = $self->_trim($line);

         my $reftype = ref($value_for{$top});
         if (! $reftype) { # infer!
            my ($key, $value) = $self->_kvcheck($line);
            if (defined($value)) {
               $value_for{$top} = {$key => $value};
            }
            else {
               $value_for{$top} = [ $line ];
            }
         }
         elsif ($reftype eq 'ARRAY') {
            push $value_for{$top}->@*, $self->_trim($line);
         }
         elsif ($reftype eq 'HASH') {
            my ($key, $value) = $self->_kvcheck($line);
            $value_for{$top}{$key} = $value if defined($value);
         }
         else { ... } # should never happen!
      }
   }

   $value_for{nameservers} = [];
   $value_for{glue} = {};
   for my $ns ($value_for{'Name servers'}->@*) {
      if (my ($host, $ip) = $ns =~ m{\A (\S+) \s+ \( (.*) \)\z}mxs) {
         push $value_for{nameservers}->@*, [ $host, $ip ];
         $value_for{glue}{$host} = $ip;
      }
      else {
         push $value_for{nameservers}->@*, $ns =~ s{\s.*}{}rmxs;
      }
   }

   return \%value_for;
}

1;
