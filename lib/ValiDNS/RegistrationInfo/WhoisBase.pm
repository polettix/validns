#!/usr/bin/env perl
package ValiDNS::RegistrationInfo::WhoisBase;

use v5.24;
use warnings;
use IO::Socket::INET;
use Ouch;
use Scalar::Util 'blessed';
use Try::Catch;
use Moo;
use experimental qw< signatures >;

use namespace::clean;

has _accepts => (is => 'rw', init_arg => 'accepts', required => 1);
has host => (is => 'rw', required => 1);
has port => (is => 'rw', default => 43);
has timeout => (is => 'rw', default => 5);

sub accepts ($self, $domain) {
   my $rx = $self->_accepts;
   return (! defined($rx)) || scalar($domain =~ m{$rx});
}

sub query ($self, $domain) {
   my $retval;
   try {
      ouch 400, 'unhandled' unless $self->accepts($domain);
      my $raw = $self->_whois_query($domain);
      $retval = {
         domain => $domain,
         source => blessed($self),
         endpoint => join(':', $self->host, $self->port),
         raw => $raw,
         $self->_parse_data($raw)->%*,
      };
   }
   catch {
      # do nothing for the moment
      warn "Exception: $_";
   };

   return $retval;
}

sub _kvcheck ($s, $x) { map { $s->_trim($_) } split m{:}mxs, $x, 2 }

sub _trim ($s, $x) { $x =~ s{\A\s+|\s+\z}{}rgmxs }

sub _whois_query ($self, $domain) {
   my $client = IO::Socket::INET->new(
      Blocking => 1,
      PeerHost => $self->host,
      PeerPort => $self->port,
      Proto => 'tcp',
      Timeout => $self->timeout,
   );

   my $query = ($domain =~ s{\A\s+|\s+\z}{}rgmxs) . "\x{0d}\x{0a}";
   my $sent = $client->send($query);
   ouch 500, "error sending query" unless ($sent // 0) == length($query);

   my $response = '';
   while ('necessary') {
      my $buffer;
      defined($client->recv($buffer, 512))
         or ouch 500, "error receiving data";
      last unless length($buffer);
      $response .= $buffer;
   }

   return $response;
}

1;
