#!/usr/bin/env perl
package ValiDNS::Role::Logger;

use v5.24;
use warnings;

use Log::Any ();

use Moo::Role;
use experimental 'signatures';

use namespace::clean;

has logger => (is => 'lazy');

sub _build_logger ($self) { return Log::Any->get_logger }

sub TRACE ($self, @rest) { $self->logger->trace(@rest) }
sub DEBUG ($self, @rest) { $self->logger->debug(@rest) }
sub INFO  ($self, @rest) { $self->logger->info(@rest) }
sub WARN  ($self, @rest) { $self->logger->warning(@rest) }
sub ERROR ($self, @rest) { $self->logger->error(@rest) }
sub FATAL ($self, @rest) { $self->logger->fatal(@rest) }

1;
