#!/usr/bin/env perl
package ValiDNS::RegistrationInfo;

use v5.24;
use warnings;

use FindBin '$RealBin';
use JSON::PP 'decode_json';
use Log::Any ();
use Mojo::UserAgent;
use Ouch;
use Path::Tiny;
use List::Util 'shuffle';

use Moo;
use experimental 'signatures';

use constant IANA_RDAP_BOOTSTRAP  => 'https://data.iana.org/rdap/dns.json';

sub __parse_rdap_bootstrap ($data) {
   my %rdap_for;
   for my $entry ($data->{services}->@*) {
      my ($matches, $servers) = $entry->@*;
      push(($rdap_for{__unrooted($_)} //= [])->@*, $servers->@*)
        for $matches->@*;
   }
   return \%rdap_for;
} ## end sub __parse_rdap_bootstrap

sub __rooted   ($domain) { lc($domain =~ s{\.*\z}{.}rmxs) }
sub __unrooted ($domain) { lc($domain =~ s{\.+\z}{}rmxs) }

use namespace::clean;

with 'ValiDNS::Role::Logger';

has force_refresh => (is => 'rw', default => 0);
has _rdap_for => (is => 'lazy');
has rdap_bootstrap => (is => 'ro', default => IANA_RDAP_BOOTSTRAP);
has ua      => (is => 'lazy');
has _whois_providers => (is => 'rw', init_arg => 'whois', default => sub { [] });

sub _build_ua ($self) {
   my $ua = Mojo::UserAgent->new(max_redirects => 5, connect_timeout => 2);
   $ua->proxy->detect;
   return $ua;
}

sub _build__rdap_for ($self) {
   my $bootstrap = $self->rdap_bootstrap;

   if (length($bootstrap // '')) {
      if ($bootstrap =~ m{\A http}imxs) {
         my $res = $self->ua->get($bootstrap)->res;
         return __parse_rdap_bootstrap($res->json) if $res->is_success;
      }
      else { # try as a local path
         my $path = path($bootstrap);
         return __parse_rdap_bootstrap(decode_json($path->slurp_raw))
            if $path->exists;
      }
   }

   $self->WARN('no RDAP info available *at all*');
   return {};    # got nothing...
} ## end sub _build__rdap_for

sub rdaps_for ($self, $domain) {
   my @parts = split m{\.}mxs, __unrooted($domain);
   my $rf    = $self->_rdap_for;
   while (@parts) {
      my $candidate = join '.', @parts;
      if (defined(my $rdaps = $rf->{$candidate})) {
         return $rdaps->@*;
      }
      shift @parts;
   } ## end while (@parts)
   return;
} ## end sub rdaps_for

sub rdap_info_for ($self, $domain) {
   $self->DEBUG('RDAP');
   my @servers = $self->rdaps_for($domain) or return;
   my $ua      = $self->ua;
   for my $server (shuffle(@servers)) {
      my $ep  = $server =~ s{/*\z}{}rmxs;
      my $url = "$ep/domain/" . __unrooted($domain);
      $self->DEBUG("trying $url");
      my $res = $ua->get($url)->res;
      next unless $res->is_success;

      my $data = $res->json;
      my %retval = (
         domain      => $domain,
         source      => 'rdap',
         rdap        => $server,
         status      => $data->{status},
         nameservers => [
            map {
               my ($oc, $name) = $_->@{qw< objectClassName ldhName >};
               (lc($oc) eq 'nameserver') ? lc($name) : ();
            } $data->{nameservers}->@*
         ],
      );
      for my $event ($data->{events}->@*) {
         my ($action, $date) = $event->@{qw< eventAction eventDate >};
         if ($action eq 'expiration') {
            $retval{expires} = $date;
         }
         elsif ($action eq 'last changed') {
            $retval{changed} = $date;
         }
      }
      return \%retval;
   } ## end for my $server (shuffle...)
   return;
} ## end sub rdap_info_for

sub fetch ($self, $domain) {
   $self->DEBUG("fetch('$domain')");
   if (my $ri = $self->rdap_info_for($domain))  { return $ri }
   if (my $wi = $self->whois_info_for($domain)) { return $wi }
   ouch 400, "no fallback for '$domain'";
} ## end sub fetch

sub whois_providers ($self) { return $self->_whois_providers->@* }

sub whois_info_for ($self, $domain) {
   $domain = __unrooted($domain);
   for my $provider ($self->whois_providers) {
      my ($type, $opts) = $provider->@*;
      $self->DEBUG("WHOIS: $type");
      my $cb = $self->can("_whois_$type") or do {
         $self->ERROR("WHOIS API for $type not supported");
         next;
      };
      my $candidate = $self->$cb($domain, $opts) or next;
      return $candidate;
   }
   return;
} ## end sub whois_info_for

sub _whois_parsed ($self, $domain, $opts) {
   my %opts = $opts->%*;
   my $class = delete $opts{class};
   $class = 'ValiDNS::RegistrationInfo::' . $class if $class !~ m{::}mxs;
   my $path = ($class . '.pm') =~ s{::}{/}rgmxs;
   require $path;
   my $instance = $class->new(%opts);
   return unless $instance->accepts($domain);
   my $got = $instance->query($domain);
   use Data::Dumper; local $Data::Dumper::Indent = 1;
   $self->DEBUG("$class returns:\n", Dumper($got));
   return $got;
}

sub _whois_whoisjson ($self, $domain, $opts) {
   state $endpoint = 'https://whoisjson.com/api/v1/whois';

   my %args = (domain => $domain);
   $args{_forceRefresh} = 1 if $self->force_refresh;
   my $res = $self->ua->get(
      $endpoint,
      {Authorization => "TOKEN=$opts->{key}"},
      form => \%args,
   )->res;
   return unless $res->is_success;
   my $data = $res->json;
   return if defined($data->{statusCode}) && $data->{statusCode} != 200;
   my $status = $data->{status};
   $status = [ $status ] unless ref($status);
   return {
      domain => $domain,
      source => 'whoisjson.com',
      endpoint => $endpoint,
      status => $status,
      nameservers => $data->{nameserver},
      available => $res->headers->header('remaining-requests'),
      changed => $data->{changed},
      expires => $data->{expires},
   };
} ## end sub _whois_whoisjson

sub _whois_ip2location ($self, $domain, $opts) {
   state $endpoint = 'https://api.ip2whois.com/v2';

   my $key = $opts->{key};

   my %args = (domain => $domain, key => $opts->{key});
   my $res = $self->ua->get($endpoint, {}, form => \%args)->res;
   return unless $res->is_success;
   my $data = $res->json;
   return if defined($data->{error});
   my $status = $data->{status};
   $status = [ $status ] unless ref($status);
   return {
      domain => $domain,
      source => 'ip2location.io',
      endpoint => $endpoint,
      status => $status,
      nameservers => $data->{nameservers},
      changed => $data->{update_date},
      expires => $data->{expire_date},
   };
}

sub _whois_localcache ($self, $domain, $opts) {
   my $path = $opts->{path};

   $path = path($path)->child($domain . '.json');
   return unless $path->exists;
   my $data = decode_json($path->slurp_raw);
   $data->{source} = 'localcache';
   $data->{dir} = "$path";
   return $data;
}

1;
