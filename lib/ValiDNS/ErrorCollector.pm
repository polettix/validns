#!/usr/bin/env perl
package ValiDNS::ErrorsList;

use v5.24;
use warnings;
use Storable 'dclone';

use Moo;
use experimental 'signatures';

use namespace::clean;

has _errors => (is => 'ro', init_arg => undef, default => sub { [] });

sub add       ($self, $item)  { push $self->_errors->@*, $item; $self }
sub all       ($self)         { return dclone($self->_errors)->@* }
sub add_array ($self, @stuff) { $self->error_item(\@stuff) }
sub add_hash  ($self, %stuff) { $self->error_item(\%stuff) }
sub is_empty  ($self)         { return scalar($self->_errors->@*) == 0 }
sub size      ($self)         { return scalar($self->_errors->@*) }

1;
